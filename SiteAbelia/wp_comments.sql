-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 07-Mar-2021 às 23:39
-- Versão do servidor: 8.0.22
-- versão do PHP: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `bitnami_wordpress`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint UNSIGNED NOT NULL,
  `comment_post_ID` bigint UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2021-02-10 12:14:31', '2021-02-10 12:14:31', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, 'post-trashed', '', 'comment', 0, 0),
(2, 8, 'Um comentarista do WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2021-02-09 09:48:21', '2021-02-09 12:48:21', 'Olá, isso é um comentário.\nPara começar a moderar, editar e excluir comentários, visite a tela de Comentários no painel.\nAvatares de comentaristas vêm a partir do <a href=\\\"https://gravatar.com\\\" rel=\\\"nofollow ugc\\\">Gravatar</a>.', 0, '1', '', 'comment', 0, 0),
(3, 18, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:23:59', '2017-07-19 08:23:59', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, 'post-trashed', '', 'comment', 0, 0),
(4, 18, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:24:06', '2017-07-19 08:24:06', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, 'post-trashed', '', 'comment', 3, 0),
(5, 18, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:24:13', '2017-07-19 08:24:13', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, 'post-trashed', '', 'comment', 0, 0),
(6, 20, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:23:09', '2017-07-19 08:23:09', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, '1', '', 'comment', 0, 0),
(7, 20, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:23:17', '2017-07-19 08:23:17', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, '1', '', 'comment', 6, 0),
(8, 20, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:23:27', '2017-07-19 08:23:27', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, '1', '', 'comment', 0, 0),
(9, 22, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:21:26', '2017-07-19 08:21:26', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, '1', '', 'comment', 0, 0),
(10, 22, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:21:34', '2017-07-19 08:21:34', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, '1', '', 'comment', 9, 0),
(11, 22, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:21:41', '2017-07-19 08:21:41', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, '1', '', 'comment', 0, 0),
(12, 26, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:17:20', '2017-07-19 08:17:20', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, '1', '', 'comment', 0, 0),
(13, 26, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:17:27', '2017-07-19 08:17:27', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, '1', '', 'comment', 12, 0),
(14, 26, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:17:34', '2017-07-19 08:17:34', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, '1', '', 'comment', 0, 0),
(15, 32, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:27:57', '2017-07-19 08:27:57', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, '1', '', 'comment', 0, 0),
(16, 32, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:28:27', '2017-07-19 08:28:27', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, '1', '', 'comment', 15, 0),
(17, 32, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:28:48', '2017-07-19 08:28:48', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, '1', '', 'comment', 0, 0),
(18, 815, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:28:11', '2017-07-19 08:28:11', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, 'post-trashed', '', 'comment', 0, 0),
(19, 815, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:28:33', '2017-07-19 08:28:33', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, 'post-trashed', '', 'comment', 18, 0),
(20, 815, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:28:52', '2017-07-19 08:28:52', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, 'post-trashed', '', 'comment', 0, 0),
(21, 822, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:28:04', '2017-07-19 08:28:04', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, 'post-trashed', '', 'comment', 0, 0),
(22, 822, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:28:37', '2017-07-19 08:28:37', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, 'post-trashed', '', 'comment', 21, 0),
(23, 822, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:28:54', '2017-07-19 08:28:54', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, 'post-trashed', '', 'comment', 0, 0),
(24, 825, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:26:12', '2017-07-19 08:26:12', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, 'post-trashed', '', 'comment', 0, 0),
(25, 825, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:26:18', '2017-07-19 08:26:18', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, 'post-trashed', '', 'comment', 24, 0),
(26, 825, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:26:29', '2017-07-19 08:26:29', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, 'post-trashed', '', 'comment', 0, 0),
(27, 1406, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:27:52', '2017-07-19 08:27:52', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, 'post-trashed', '', 'comment', 0, 0),
(28, 1406, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:28:24', '2017-07-19 08:28:24', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, 'post-trashed', '', 'comment', 27, 0),
(29, 1406, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:28:46', '2017-07-19 08:28:46', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, 'post-trashed', '', 'comment', 0, 0),
(30, 1407, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:20:43', '2017-07-19 08:20:43', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, 'post-trashed', '', 'comment', 0, 0),
(31, 1407, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:20:50', '2017-07-19 08:20:50', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, 'post-trashed', '', 'comment', 30, 0),
(32, 1407, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:20:57', '2017-07-19 08:20:57', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, 'post-trashed', '', 'comment', 0, 0),
(33, 1408, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:27:49', '2017-07-19 08:27:49', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, '1', '', 'comment', 0, 0),
(34, 1408, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:28:24', '2017-07-19 08:28:24', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, '1', '', 'comment', 33, 0),
(35, 1408, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:28:44', '2017-07-19 08:28:44', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, '1', '', 'comment', 0, 0),
(36, 1409, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:22:18', '2017-07-19 08:22:18', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, '1', '', 'comment', 0, 0),
(37, 1409, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:22:26', '2017-07-19 08:22:26', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, '1', '', 'comment', 36, 0),
(38, 1409, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:22:33', '2017-07-19 08:22:33', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, '1', '', 'comment', 0, 0),
(39, 1410, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:27:45', '2017-07-19 08:27:45', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, '1', '', 'comment', 0, 0),
(40, 1410, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:28:19', '2017-07-19 08:28:19', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, '1', '', 'comment', 39, 0),
(41, 1410, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:28:43', '2017-07-19 08:28:43', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, '1', '', 'comment', 0, 0),
(42, 1413, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:14:31', '2017-07-19 08:14:31', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, 'post-trashed', '', 'comment', 0, 0),
(43, 1413, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:14:51', '2017-07-19 08:14:51', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, 'post-trashed', '', 'comment', 42, 0),
(44, 1413, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:14:58', '2017-07-19 08:14:58', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, 'post-trashed', '', 'comment', 0, 0),
(45, 1418, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:11:59', '2017-07-19 08:11:59', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, '1', '', 'comment', 0, 0),
(46, 1418, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:12:06', '2017-07-19 08:12:06', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, '1', '', 'comment', 45, 0),
(47, 1418, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:12:13', '2017-07-19 08:12:13', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, '1', '', 'comment', 0, 0),
(48, 1419, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:10:55', '2017-07-19 08:10:55', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, '1', '', 'comment', 0, 0),
(49, 1419, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:11:02', '2017-07-19 08:11:02', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, '1', '', 'comment', 48, 0),
(50, 1419, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:11:10', '2017-07-19 08:11:10', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, '1', '', 'comment', 0, 0),
(51, 24, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:18:03', '2017-07-19 08:18:03', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, 'post-trashed', '', 'comment', 0, 0),
(52, 24, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:18:10', '2017-07-19 08:18:10', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, 'post-trashed', '', 'comment', 51, 0),
(53, 24, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:18:17', '2017-07-19 08:18:17', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, 'post-trashed', '', 'comment', 0, 0),
(54, 28, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:19:58', '2017-07-19 08:19:58', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, 'post-trashed', '', 'comment', 0, 0),
(55, 28, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:20:06', '2017-07-19 08:20:06', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, 'post-trashed', '', 'comment', 54, 0),
(56, 28, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:20:14', '2017-07-19 08:20:14', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, 'post-trashed', '', 'comment', 0, 0),
(57, 34, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:28:07', '2017-07-19 08:28:07', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, 'post-trashed', '', 'comment', 0, 0),
(58, 34, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:28:31', '2017-07-19 08:28:31', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, 'post-trashed', '', 'comment', 57, 0),
(59, 34, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:28:50', '2017-07-19 08:28:50', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, 'post-trashed', '', 'comment', 0, 0),
(60, 52, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:10:16', '2017-07-19 08:10:16', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, 'post-trashed', '', 'comment', 0, 0),
(61, 52, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:10:23', '2017-07-19 08:10:23', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, 'post-trashed', '', 'comment', 60, 0),
(62, 52, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:10:29', '2017-07-19 08:10:29', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, 'post-trashed', '', 'comment', 0, 0),
(63, 54, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:09:02', '2017-07-19 08:09:02', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, 'post-trashed', '', 'comment', 0, 0),
(64, 54, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:09:12', '2017-07-19 08:09:12', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, 'post-trashed', '', 'comment', 63, 0),
(65, 54, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:09:18', '2017-07-19 08:09:18', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, 'post-trashed', '', 'comment', 0, 0),
(66, 819, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:07:33', '2017-07-19 08:07:33', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, 'post-trashed', '', 'comment', 0, 0),
(67, 819, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:07:40', '2017-07-19 08:07:40', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, 'post-trashed', '', 'comment', 66, 0),
(68, 819, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:07:49', '2017-07-19 08:07:49', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, 'post-trashed', '', 'comment', 0, 0),
(69, 1411, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:19:05', '2017-07-19 08:19:05', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, 'post-trashed', '', 'comment', 0, 0),
(70, 1411, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:19:12', '2017-07-19 08:19:12', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, 'post-trashed', '', 'comment', 69, 0),
(71, 1411, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:19:19', '2017-07-19 08:19:19', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, 'post-trashed', '', 'comment', 0, 0),
(72, 1412, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:16:29', '2017-07-19 08:16:29', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, 'post-trashed', '', 'comment', 0, 0),
(73, 1412, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:16:36', '2017-07-19 08:16:36', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, 'post-trashed', '', 'comment', 72, 0),
(74, 1412, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:16:46', '2017-07-19 08:16:46', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, 'post-trashed', '', 'comment', 0, 0),
(75, 1414, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:16:00', '2017-07-19 08:16:00', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, 'post-trashed', '', 'comment', 0, 0),
(76, 1414, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:16:07', '2017-07-19 08:16:07', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, 'post-trashed', '', 'comment', 75, 0),
(77, 1414, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:16:14', '2017-07-19 08:16:14', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, 'post-trashed', '', 'comment', 0, 0),
(78, 1415, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:13:44', '2017-07-19 08:13:44', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, 'post-trashed', '', 'comment', 0, 0),
(79, 1415, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:13:51', '2017-07-19 08:13:51', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, 'post-trashed', '', 'comment', 78, 0),
(80, 1415, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:13:57', '2017-07-19 08:13:57', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, 'post-trashed', '', 'comment', 0, 0),
(81, 1416, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:13:21', '2017-07-19 08:13:21', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, 'post-trashed', '', 'comment', 0, 0),
(82, 1416, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:13:28', '2017-07-19 08:13:28', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, 'post-trashed', '', 'comment', 81, 0),
(83, 1416, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:13:34', '2017-07-19 08:13:34', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, 'post-trashed', '', 'comment', 0, 0),
(84, 1417, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:12:37', '2017-07-19 08:12:37', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.', 0, 'post-trashed', '', 'comment', 0, 0),
(85, 1417, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:12:43', '2017-07-19 08:12:43', 'Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil.', 0, 'post-trashed', '', 'comment', 84, 0),
(86, 1417, 'Shawn Roberts', 'breaking.pencidesign@gmail.com', 'http://pencidesign.com', '123.24.178.5', '2017-07-19 08:12:50', '2017-07-19 08:12:50', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.', 0, 'post-trashed', '', 'comment', 0, 0);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
